Project: iOS Getting Started. Platform: iOS 10.0. Language: Swift 3.0.

This is my version of the Apple "Getting Started" iOS development guide (https://developer.apple.com/library/content/referencelibrary/GettingStarted/DevelopiOSAppsSwift/).
It teaches the basics of creating an iOS app with Swift. The created app is a list of meals persisted in phone memory that have a name, a photo, and a rating.
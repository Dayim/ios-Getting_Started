//
//  Tutorial_FoodTrackerTests.swift
//  Tutorial-FoodTrackerTests
//
//  Created by Yim, David | Men | ECLD on 9/20/16.
//  Copyright © 2016 Yim, David | Men | ECLD. All rights reserved.
//

import UIKit
import XCTest

class Tutorial_FoodTrackerTests: XCTestCase {
    
   // MARK: FoodTracker Tests
    func testMealInitialization() {
        let potentialItem = Meal(name: "Nano desu", photo: nil, rating: 5)
        XCTAssertNotNil(potentialItem)
        
        let akb0048 = Meal(name: "", photo: nil, rating: 0)
        XCTAssertNil(akb0048, "Empty name is invalid")
        
        let badRating = Meal(name: "This meal sucks", photo: nil, rating:-1)
        XCTAssertNil(badRating, "Negative ratings are invalid you bastard!")
    }
    
}

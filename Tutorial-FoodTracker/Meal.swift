//
//  Meal.swift
//  Tutorial-FoodTracker
//
//  Created by Yim, David | Men | ECLD on 9/28/16.
//  Copyright © 2016 Yim, David | Men | ECLD. All rights reserved.
//

import UIKit
import os.log

// Subclass of NSObject and conforms NSCoding protocol to be able to persist
class Meal : NSObject, NSCoding {
    
    // MARK: Types
    // This struct is a mapping for NSCoding persistence
    // The static keyword has the same meaning as in Java
    struct PropertyKey {
        static let name = "name"
        static let photo = "photo"
        static let rating = "rating"
    }
    
    // MARK: Properties
    // A property marked with ? is optional
    var name: String
    var photo: UIImage?
    var rating: Int
    
    // MARK: Archiving Paths
    // This looks up the URL to which the app's documents directory where it can save data. It uses first, but should always return one object.
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("meals")
    
    // MARK: Initilization
    // The ? indicates that this can return nil
    init?(name: String, photo: UIImage?, rating: Int) {
        if name.isEmpty || rating < 0 {
            return nil
        }
        self.name = name
        self.photo = photo
        self.rating = rating
    }
    
    // MARK: NSCoding
    
    // This persist the data into a map
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: PropertyKey.name)
        aCoder.encode(photo, forKey: PropertyKey.photo)
        aCoder.encode(rating, forKey: PropertyKey.rating)
    }
    
    // This gets the persisted data
    required convenience init?(coder aDecoder: NSCoder) {
        // The name is required. If we cannot decode a name string, the initializer should fail.
        guard let name = aDecoder.decodeObject(forKey: PropertyKey.name) as? String else {
            //os_log("Unable to decode the name for a Meal object.", log: OSLog.default, type: .debug)
            return nil
        }
        
        // Because photo is an optional property of Meal, just use conditional cast.
        let photo = aDecoder.decodeObject(forKey: PropertyKey.photo) as? UIImage
        
        // Because the return value of decodeIntegerForKey is Int, there’s no need to downcast the decoded value and there is no optional to unwrap.
        let rating = aDecoder.decodeInteger(forKey: PropertyKey.rating)
        
        // Must call designated initializer.
        self.init(name: name, photo: photo, rating: rating)
    }
}
